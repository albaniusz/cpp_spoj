#include <stdio.h>

int main()
{
	int i, result = 0;
	long n, d, c, sum = 0;

	scanf("%d %d", &n, &d);

	if (n > 10000000)
	{
		return 1;
	}
	
	if (d < 1 || d > 1000000000)
	{
		return 1;
	}

	for (i = 0; i < n; i++)
	{
		scanf("%l", &c);
		sum += c;
	}

	if (c > 1000000000)
	{
		return 1;
	}

	if (d == 1)
	{
		printf("%d\n", sum);
		return 0;
	}

	char marker = 0;
	while (marker = 0)
	{
		sum = sum / d;
		if (sum == 0)
		{
			marker = 1;
		}

		result++;
	}
	result++;

	printf("%d\n", result);

	return 0;
}

